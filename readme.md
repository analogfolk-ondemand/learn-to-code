Learning to code
================



Why?
----

Because it's easy and fun!



“Learning to write programs stretches your mind, and helps you think better,
creates a way of thinking about things that I think is helpful in all domains.“

**Bill Gates**

“At a time when people are saying "I want a good job - I got out of college and
I couldnt find one," every single year in America there is a standing demand for
120,000 people who are training in computer science.“

**Bill Clinton**

“Here we are, 2013, we ALL depend on technology to communicate, to bank, and
none of us know how to read and write code. It's important for these kids, right
now, starting at 8 years old, to read and write code.“

**will.i.am**

“Our policy at Facebook is literally to hire as many talented engineers as we
can find. There just aren't enough people who are trained and have these skills
today.“

**Mark Zuckerberg**

“Whether we're fighting climate change or going to space, everything is moved
forward by computers, and we don't have enough people who can code. Teaching
young people to code early on can help build skills and confidence and energize
the classroom with learning-by-doing opportunities. I learned how to fly a hot
air balloon when I was 30,000 feet up and my life was in the balance: you can
learn skills at any age but why wait when we can teach everyone to code now!“

**Richard Branson**



How?
----

Learning to code doesn't require any expensive software, products or
memberships. At the bare minimum all you need is a web browser.

There are many sites designed for the sole purpose of practising and playing
around with snippets of code. For example [www.codepen.io][5] allows you to
create a new 'pen' and add your own HTML, CSS and JavaScript, and try it out in
your browser for free.

[5]: <http://codepen.io/>



Inspiration
-----------

-   Jennifer Dewalt - [180 websites in 180 days][1]

-   [TED talk][15] - 10 places where anyone can learn to code

[1]: <http://blog.jenniferdewalt.com/post/56319597560/im-learning-to-code-by-building-180-websites-in-180>

[15]: <http://blog.ted.com/2013/01/29/10-places-where-anyone-can-learn-to-code/>



Tutorials
---------

Tutorials are available for developers of all levels of experience.



-   [Mozilla HTML, CSS and JavaScript tutorials][3]

-   [Codecademy][4] - Interactive introductions for many of the most popular web
    development languages.

-   [Khan Academy][7] - Learn the fundamentals of computer science, online, for
    free.

-   [Udacity][8] - Courses on everything from computer science through to
    Physics and genetics.

-   [Code.org][14] - more interactive online tutorials

[14]: <http://code.org/>

-   [Web acronyms explained][13]

[3]: <<https://developer.mozilla.org/en-US/docs/Web/Tutorials>

[4]: <http://www.codecademy.com/>

[7]: <https://www.khanacademy.org/cs>

[8]: <https://www.udacity.com/>

[13]: <http://www.codeconquest.com/web-acronym/>



Resources
---------

-   [w3 Consortium][11] - The World Wide Web Consortium is led by the founder of
    the internet, Tim Berners-Lee and is responsible for developing and
    maintaining the web standards that help keep web development on track.

[11]: <http://www.w3.org/>

-   [Mozilla developer network][2] - MDN is a great resource for finding out the
    semantics, best use and examples of nearly everything you will encounter
    building a basic website.

-   [Stack Overflow][6] - A hero site for many developers. Stack Overflow has
    been around for about 5 years and is one of the biggest question and answer
    forums for coding and programming online. If you run in to a problem, there
    is a pretty good chance someone else has already encountered the same
    problem and asked the question on stack overflow.

-   [CSS-tricks.com][12] - A huge site of code snippets that is maintained by
    some of the top developers around.

[2]: <https://developer.mozilla.org/en-US/>

[6]: <http://stackoverflow.com/>

[12]: <http://css-tricks.com/>



Tools
-----

-   [Sublime text][9] - A text editor with more than enough capability to build
    even the most complex web apps. The more you use it the more features you
    will discover. Many developers swear by sublime text.

-   [Twitter bootstrap][10] - a front end framework designed to take care of the
    heavy lifting, so that you can get your site live faster.

[9]: <http://www.sublimetext.com/2>

[10]: <http://getbootstrap.com/>



AVOID
=====

At all costs, avoid w3schools.com. This website presents itself as being a part
of the w3 consortium, but it isn't. A lot of the articles are old and out of
date, or flat out incorrect.
